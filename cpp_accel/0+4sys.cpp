#include "0+4sys.h"

constexpr double mass = 52; // kg
constexpr double width = 0.75; // kg
constexpr double length = 1.25; // m
constexpr double moi = 6; // kg-m^2

constexpr double wheel_radius = (6. / 39.37) / 2.; // 4" wheels
constexpr double wheelbase_width = 0.6;
constexpr double wheelbase_length = 1;
constexpr double b_fwd = 10;
constexpr double b_lat = 20;

constexpr double max_voltage = 12;
constexpr double nominal_voltage = 12;
constexpr double gear_ratio = 7;
constexpr double free_speed = (5330. / 60.) * 2 * M_PI;
constexpr double _free_current = 2.7;
constexpr double _stall_current = 133.;
constexpr double _stall_torque = 2.41;

constexpr double num_motors_per_gearbox = 2;

constexpr double free_current = _free_current * num_motors_per_gearbox;
constexpr double stall_current = _stall_current * num_motors_per_gearbox;
constexpr double stall_torque = _stall_torque * num_motors_per_gearbox;

constexpr double motor_resistance = nominal_voltage / stall_current;
constexpr double kv = free_speed / (nominal_voltage - motor_resistance * free_current);
constexpr double kt = stall_torque / stall_current;

template<typename T>
RobotSystem<T>::RobotSystem(drake::systems::SystemScalarConverter converter) :
        drake::systems::LeafSystem<T>(std::move(converter)) {
    this->DeclareInputPort("motor_voltage", drake::systems::kVectorValued, 2);
    //this->DeclareContinuousState(3,3,0);
    this->DeclareContinuousState(6);
    this->DeclareVectorOutputPort("state",
                                  drake::systems::BasicVector<T>(6),
                                  &RobotSystem::CopyStateOut,
                                  {this->all_sources_ticket()});
}

template<typename T>
void RobotSystem<T>::CopyStateOut(const drake::systems::Context<T> &context,
                                  drake::systems::BasicVector<T> *output) const {
    const drake::systems::VectorBase<T> &continuous_state_vector =
            context.get_continuous_state_vector();

    output->set_value(continuous_state_vector.CopyToVector());
}

//template <typename T>
//template <typename U>
//RobotSystem<T>::RobotSystem(const RobotSystem<U>& other) : RobotSystem<T>() {}

template<typename T>
RobotSystem<T>::~RobotSystem<T>() {}

template<typename T>
void RobotSystem<T>::DoCalcTimeDerivatives(const drake::systems::Context<T> &context,
                                           drake::systems::ContinuousState<T> *derivatives) const {
    const drake::systems::VectorBase<T> &continuous_state_vector = context.get_continuous_state_vector();
    const drake::systems::BasicVector<T> *input_vector = this->EvalVectorInput(context, 0);
    drake::systems::VectorBase<T> &derivative_vector = derivatives->get_mutable_vector();


    const drake::Vector2<T> u = input_vector->get_value();
    const drake::Vector6<T> x = continuous_state_vector.CopyToVector();
    drake::Vector6<T> dx;

    f(x, u, &dx);


}

template<typename T>
//void RobotSystem<T>::f(const drake::systems::VectorBase<T> &x, const drake::systems::VectorBase<T> &u,
//                       drake::systems::VectorBase<T> &dx) const {
void RobotSystem<T>::f(const drake::Vector6<T> &x, const drake::Vector2<T> &u, drake::Vector6<T> *dx) const {

    T motor_voltage_l = u[0];
    T motor_voltage_r = u[1];

    T pos_xdot = x[0];
    T pos_ydot = x[1];
    T pos_thetadot = x[2];
    T pos_x = x[3];
    T pos_y = x[4];
    T pos_theta = x[5];

    T c = cos(-pos_theta);
    T s = sin(-pos_theta);

    T fwd_vel = c * pos_xdot + -s * pos_ydot;
    T lat_vel = s * pos_xdot + c * pos_ydot;

    T fwd_ffriction = fwd_vel * b_fwd;
    T lat_ffriction = lat_vel * b_lat;
    T fwd_afriction = fwd_ffriction / mass;
    T lat_afriction = lat_ffriction / mass;

    T ss_l = fwd_vel - pos_thetadot * wheelbase_width;
    T ss_r = fwd_vel + pos_thetadot * wheelbase_width;

    T motor_rpm_l = (ss_l / wheel_radius) * gear_ratio;
    T motor_rpm_r = (ss_r / wheel_radius) * gear_ratio;

    T motor_bemf_l = motor_rpm_l * 1 / kv;
    T motor_armature_v_l = motor_voltage_l - motor_bemf_l;
    T motor_current_l = motor_armature_v_l / motor_resistance;
    T motor_torque_l = motor_current_l * kt;
    T wheel_torque_l = motor_torque_l * gear_ratio;
    T wheel_force_l = wheel_torque_l / wheel_radius;

    T motor_bemf_r = motor_rpm_r * 1 / kv;
    T motor_armature_v_r = motor_voltage_r - motor_bemf_r;
    T motor_current_r = motor_armature_v_r / motor_resistance;
    T motor_torque_r = motor_current_r * kt;
    T wheel_torque_r = motor_torque_r * gear_ratio;
    T wheel_force_r = wheel_torque_r / wheel_radius;

    T fwd_accel = (wheel_force_l + wheel_force_r) / mass;

    T xddot = (fwd_accel - fwd_afriction) * cos(pos_theta) - lat_afriction * cos(pos_theta + M_PI / 2);
    T yddot = (fwd_accel - fwd_afriction) * sin(pos_theta) - lat_afriction * sin(pos_theta + M_PI / 2);

    T radial_accel = (wheel_force_r - wheel_force_l) * wheelbase_width / moi;
    T thetaddot = radial_accel;


    (*dx)[0] = xddot;
    (*dx)[1] = yddot;
    (*dx)[2] = thetaddot;
    (*dx)[3] = pos_xdot;
    (*dx)[4] = pos_ydot;
    (*dx)[5] = pos_thetadot;
}

DRAKE_DEFINE_CLASS_TEMPLATE_INSTANTIATIONS_ON_DEFAULT_SCALARS(
        class ::RobotSystem)