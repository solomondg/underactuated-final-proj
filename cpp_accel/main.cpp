#include <iostream>
#include <stdexcept>

#include <drake/common/find_resource.h>
#include <drake/common/drake_copyable.h>
#include <drake/common/eigen_types.h>
#include <drake/geometry/drake_visualizer.h>
#include <drake/geometry/scene_graph.h>
#include <drake/systems/analysis/simulator.h>
#include <drake/systems/framework/context.h>
#include <drake/systems/framework/diagram.h>
#include <drake/systems/framework/diagram_builder.h>
#include <drake/systems/framework/vector_base.h>
#include <drake/systems/primitives/constant_vector_source.h>

#include <drake/solvers/solve.h>
#include <drake/systems/trajectory_optimization/direct_collocation.h>

#include <drake/math/autodiff.h>

#include <Eigen/Dense>

#include "0+4sys.h"

using namespace drake;
using namespace drake::systems;

using Eigen::Vector2d;
using Eigen::Vector3d;
using Eigen::Vector4d;
using Eigen::VectorXd;

template <typename T>
class TestSystem : public LeafSystem<T> {
public:
    TestSystem() {
        this->set_name("TestSystem");
        this->DeclareNumericParameter(BasicVector<T>{13.0, 7.0});
        this->DeclareAbstractParameter(Value<std::string>("parameter value"));

        this->DeclareDiscreteState(3);
        this->DeclareAbstractState(Value<int>(5));
        this->DeclareAbstractState(Value<std::string>("second abstract state"));
    }
    ~TestSystem() override {}

    using LeafSystem<T>::DeclareContinuousState;
    using LeafSystem<T>::DeclareDiscreteState;
    using LeafSystem<T>::DeclareNumericParameter;
    using LeafSystem<T>::DeclareVectorInputPort;
    using LeafSystem<T>::DeclareAbstractInputPort;
    using LeafSystem<T>::DeclareVectorOutputPort;
    using LeafSystem<T>::DeclareAbstractOutputPort;
    using LeafSystem<T>::DeclarePerStepEvent;

    void AddPeriodicUpdate() {
        const double period = 10.0;
        const double offset = 5.0;
        this->DeclarePeriodicDiscreteUpdate(period, offset);
        std::optional<PeriodicEventData> periodic_attr =
                this->GetUniquePeriodicDiscreteUpdateAttribute();
    }

    void AddPeriodicUpdate(double period) {
        const double offset = 0.0;
        this->DeclarePeriodicDiscreteUpdate(period, offset);
        std::optional<PeriodicEventData> periodic_attr =
                this->GetUniquePeriodicDiscreteUpdateAttribute();
    }

    void AddPeriodicUpdate(double period, double offset) {
        this->DeclarePeriodicDiscreteUpdate(period, offset);
    }

    void AddPeriodicUnrestrictedUpdate(double period, double offset) {
        this->DeclarePeriodicUnrestrictedUpdate(period, offset);
    }

    void AddPublish(double period) { this->DeclarePeriodicPublish(period); }

    void DoCalcTimeDerivatives(const Context<T>& context,
                               ContinuousState<T>* derivatives) const override {}

    void CalcOutput(const Context<T>& context, BasicVector<T>* output) const {}

    const BasicVector<T>& GetVanillaNumericParameters(
            const Context<T>& context) const {
        return this->GetNumericParameter(context, 0 /* index */);
    }

    BasicVector<T>& GetVanillaMutableNumericParameters(
            Context<T>* context) const {
        return this->GetMutableNumericParameter(context, 0 /* index */);
    }

    // First testing type: no event specified.
    std::unique_ptr<WitnessFunction<T>> MakeWitnessWithoutEvent() const {
        return this->MakeWitnessFunction(
                "dummy1", WitnessFunctionDirection::kCrossesZero,
                &TestSystem<double>::DummyWitnessFunction);
    }

    // Second testing type: event specified.
    std::unique_ptr<WitnessFunction<T>> MakeWitnessWithEvent() const {
        return this->MakeWitnessFunction(
                "dummy2", WitnessFunctionDirection::kNone,
                &TestSystem<double>::DummyWitnessFunction,
                PublishEvent<double>());
    }

    // Third testing type: publish callback specified.
    std::unique_ptr<WitnessFunction<T>> MakeWitnessWithPublish() const {
        return this->MakeWitnessFunction(
                "dummy3", WitnessFunctionDirection::kNone,
                &TestSystem<double>::DummyWitnessFunction,
                &TestSystem<double>::PublishCallback);
    }

    // Fourth testing type: discrete update callback specified.
    std::unique_ptr<WitnessFunction<T>> MakeWitnessWithDiscreteUpdate() const {
        return this->MakeWitnessFunction(
                "dummy4", WitnessFunctionDirection::kNone,
                &TestSystem<double>::DummyWitnessFunction,
                &TestSystem<double>::DiscreteUpdateCallback);
    }

    // Fifth testing type: unrestricted update callback specified.
    std::unique_ptr<WitnessFunction<T>>
    MakeWitnessWithUnrestrictedUpdate() const {
        return this->MakeWitnessFunction(
                "dummy5", WitnessFunctionDirection::kNone,
                &TestSystem<double>::DummyWitnessFunction,
                &TestSystem<double>::UnrestrictedUpdateCallback);
    }

    // Sixth testing type: lambda function with no event specified.
    std::unique_ptr<WitnessFunction<T>> DeclareLambdaWitnessWithoutEvent() const {
        return this->MakeWitnessFunction(
                "dummy6", WitnessFunctionDirection::kCrossesZero,
                [](const Context<double>&) -> double { return 7.0; });
    }

    // Seventh testing type: lambda function with event specified.
    std::unique_ptr<WitnessFunction<T>>
    DeclareLambdaWitnessWithUnrestrictedUpdate() const {
        return this->MakeWitnessFunction(
                "dummy7", WitnessFunctionDirection::kPositiveThenNonPositive,
                [](const Context<double>&) -> double { return 11.0; },
                UnrestrictedUpdateEvent<double>());
    }

    // Indicates whether various callbacks have been called.
    bool publish_callback_called() const { return publish_callback_called_; }
    bool discrete_update_callback_called() const {
        return discrete_update_callback_called_;
    }
    bool unrestricted_update_callback_called() const {
        return unrestricted_update_callback_called_;
    }

    // Verifies that the forced publish events collection has been allocated.
    bool forced_publish_events_collection_allocated() const {
        return this->forced_publish_events_exist();
    }

    // Verifies that the forced discrete update events collection has been
    // allocated.
    bool forced_discrete_update_events_collection_allocated() const {
        return this->forced_discrete_update_events_exist();
    }

    // Verifies that the forced unrestricted update events collection has been=
    // allocated.
    bool forced_unrestricted_update_events_collection_allocated() const {
        return this->forced_unrestricted_update_events_exist();
    }

    // Gets the forced publish events collection.
    const EventCollection<PublishEvent<double>>&
    get_forced_publish_events_collection() const {
        return this->get_forced_publish_events();
    }

    // Gets the forced discrete update events collection.
    const EventCollection<DiscreteUpdateEvent<double>>&
    get_forced_discrete_update_events_collection() const {
        return this->get_forced_discrete_update_events();
    }

    // Gets the forced unrestricted update events collection.
    const EventCollection<UnrestrictedUpdateEvent<double>>&
    get_forced_unrestricted_update_events_collection() const {
        return this->get_forced_unrestricted_update_events();
    }

    // Sets the forced publish events collection.
    void set_forced_publish_events_collection(
            std::unique_ptr<EventCollection<PublishEvent<double>>>
    publish_events) {
        this->set_forced_publish_events(std::move(publish_events));
    }

    // Sets the forced discrete_update events collection.
    void set_forced_discrete_update_events_collection(
            std::unique_ptr<EventCollection<DiscreteUpdateEvent<double>>>
    discrete_update_events) {
        this->set_forced_discrete_update_events(std::move(discrete_update_events));
    }

    // Sets the forced unrestricted_update events collection.
    void set_forced_unrestricted_update_events_collection(
            std::unique_ptr<EventCollection<UnrestrictedUpdateEvent<double>>>
    unrestricted_update_events) {
        this->set_forced_unrestricted_update_events(
                std::move(unrestricted_update_events));
    }

    // Gets the mutable version of the forced publish events collection.
    EventCollection<PublishEvent<double>>&
    get_mutable_forced_publish_events_collection() {
        return this->get_mutable_forced_publish_events();
    }

private:
    // This dummy witness function exists only to test that the
    // MakeWitnessFunction() interface works as promised.
    T DummyWitnessFunction(const Context<T>& context) const {
        static int call_counter = 0;
        return static_cast<T>(++call_counter);
    }

    // Publish callback function, which serves to test whether the appropriate
    // MakeWitnessFunction() interface works as promised.
    void PublishCallback(const Context<T>&, const PublishEvent<T>&) const {
        publish_callback_called_ = true;
    }

    // Discrete update callback function, which serves to test whether the
    // appropriate MakeWitnessFunction() interface works as promised.
    void DiscreteUpdateCallback(const Context<T>&,
                                const DiscreteUpdateEvent<T>&, DiscreteValues<T>*) const {
        discrete_update_callback_called_ = true;
    }

    // Unrestricted update callback function, which serves to test whether the
    // appropriate MakeWitnessFunction() interface works as promised.
    void UnrestrictedUpdateCallback(const Context<T>&,
                                    const UnrestrictedUpdateEvent<T>&, State<T>*) const {
        unrestricted_update_callback_called_ = true;
    }

    // Indicators for which callbacks have been called, made mutable as
    // PublishCallback() cannot alter any state (the others are mutable for
    // consistency with PublishCallback() and to avoid machinations with state
    // that would otherwise be required).
    mutable bool publish_callback_called_{false};
    mutable bool discrete_update_callback_called_{false};
    mutable bool unrestricted_update_callback_called_{false};
};

template <typename T>
class SymbolicSparsitySystem : public LeafSystem<T> {
public:
    explicit SymbolicSparsitySystem(bool use_default_prereqs = true)
            : SymbolicSparsitySystem(SystemTypeTag<SymbolicSparsitySystem>{},
                                     use_default_prereqs) {}

    // Scalar-converting copy constructor.
    template <typename U>
    SymbolicSparsitySystem(const SymbolicSparsitySystem<U>& source)
            : SymbolicSparsitySystem<T>(source.is_using_default_prereqs()) {
        source.count_conversion();
    }

    // Note that this object was used as the source for a scalar conversion.
    void count_conversion() const { ++num_conversions_; }

    int num_conversions() const { return num_conversions_; }

    bool is_using_default_prereqs() const { return use_default_prereqs_; }
protected:
    explicit SymbolicSparsitySystem(SystemScalarConverter converter,
                                    bool use_default_prereqs = true)
            : LeafSystem<T>(std::move(converter)),
              use_default_prereqs_(use_default_prereqs) {
        const int kSize = 1;

        this->DeclareInputPort(kVectorValued, kSize);
        this->DeclareInputPort(kVectorValued, kSize);

        if (is_using_default_prereqs()) {
            // Don't specify prerequisites; we'll have to perform symbolic analysis
            // to determine whether there is feedthrough.
            this->DeclareVectorOutputPort(BasicVector<T>(kSize),
                                          &SymbolicSparsitySystem::CalcY0);
            this->DeclareVectorOutputPort(BasicVector<T>(kSize),
                                          &SymbolicSparsitySystem::CalcY1);
        } else {
            // Explicitly specify the prerequisites for the code in CalcY0() and
            // CalcY1() below. No need for further analysis to determine feedthrough.
            this->DeclareVectorOutputPort(
                    BasicVector<T>(kSize), &SymbolicSparsitySystem::CalcY0,
                    {this->input_port_ticket(InputPortIndex(1))});
            this->DeclareVectorOutputPort(
                    BasicVector<T>(kSize), &SymbolicSparsitySystem::CalcY1,
                    {this->input_port_ticket(InputPortIndex(0))});
        }
    }
private:
    void CalcY0(const Context<T>& context,
                BasicVector<T>* y0) const {
        const auto& u1 = this->get_input_port(1).Eval(context);
        y0->set_value(u1);
    }

    void CalcY1(const Context<T>& context,
                BasicVector<T>* y1) const {
        const auto& u0 = this->get_input_port(0).Eval(context);
        y1->set_value(u0);
    }

    const bool use_default_prereqs_;

    // Count how many times this object was used as the _source_ for the
    // conversion constructor.
    mutable int num_conversions_{0};
};




int main() {
    //drake::systems::DiagramBuilder<double> builder;
    //auto robosys = builder.AddSystem<RobotSystem<double>>();
    //auto const_vec_source = builder.AddSystem<drake::systems::ConstantVectorSource<double>>(0,0);
    //builder.connect(*const_vec_source,*robosys);

    int num_pts = 30;
    double t_end = 5;
    double max_dt = t_end / ((double) num_pts);

    const RobotSystem<double> sys = RobotSystem<double>();
    std::unique_ptr<drake::systems::Context<double>> context = sys.CreateDefaultContext();

    drake::systems::trajectory_optimization::DirectCollocation dircol = drake::systems::trajectory_optimization::DirectCollocation(
            &sys,
            *context,
            num_pts,
            0.01,
            max_dt,
            drake::systems::InputPortSelection::kUseFirstInputIfItExists,
            true
    );

    dircol.AddEqualTimeIntervalsConstraints();

    auto u = dircol.input();
    auto x = dircol.state();

    double v_bus = 12.;

    //dircol.AddConstraintToAllKnotPoints(-v_bus <= u[0]);
    //dircol.AddConstraintToAllKnotPoints(-v_bus <= u[1]);
    //dircol.AddConstraintToAllKnotPoints(u[0] <= v_bus);
    //dircol.AddConstraintToAllKnotPoints(u[1] <= v_bus);

    //dircol.AddConstraintToAllKnotPoints(-10 <= x[0]);
    //dircol.AddConstraintToAllKnotPoints(-10 <= x[1]);
    //dircol.AddConstraintToAllKnotPoints(-15 <= x[2]);
    //dircol.AddConstraintToAllKnotPoints(x[0] <= 10);
    //dircol.AddConstraintToAllKnotPoints(x[1] <= 10);
    //dircol.AddConstraintToAllKnotPoints(x[2] <= 15);

    //dircol.AddConstraintToAllKnotPoints(-20 <= x[3]);
    //dircol.AddConstraintToAllKnotPoints(-20 <= x[4]);
    //dircol.AddConstraintToAllKnotPoints(-4 * M_PI <= x[5]);
    //dircol.AddConstraintToAllKnotPoints(x[3] <= 20);
    //dircol.AddConstraintToAllKnotPoints(x[4] <= 20);
    //dircol.AddConstraintToAllKnotPoints(x[5] <= 4 * M_PI);

    Eigen::VectorXd initial_state = Eigen::VectorXd(6);
    initial_state << 0., 0., 0., 0., 0., 0.;
    dircol.AddBoundingBoxConstraint(initial_state, initial_state, dircol.initial_state());

    Eigen::VectorXd final_state = Eigen::VectorXd(6);
    final_state << 0., 0., 0., 5., 0., 0.;
    dircol.AddBoundingBoxConstraint(final_state, final_state, dircol.final_state());

    double R = 1;
    double Q = 5;

    dircol.AddRunningCost(
            R * (u[0] / v_bus) * (u[0] / v_bus) +
            R * (u[1] / v_bus) * (u[1] / v_bus)
    );

    dircol.AddRunningCost(Q);

    printf("Setting up breaks\n");
    Eigen::VectorXd breaks = Eigen::VectorXd::LinSpaced(6, 0, t_end);
    for (int i=0; i<6; i++) printf("%f ", breaks[i]);
    printf("\n");
    printf("r: %ld c: %ld\n", breaks.rows(), breaks.cols());
    printf("Setting up samples\n");
    Eigen::MatrixXd samples = Eigen::MatrixXd(6, 2);
    printf("r: %ld c: %ld\n", samples.rows(), samples.cols());
    printf("Writing to samples\n");
    samples << initial_state, final_state;

    Eigen::MatrixXd samples_t = samples.transpose();

    printf("sample cols: %ld breaks size: %ld\n", samples.cols(), breaks.size());
    printf("sample cols: %ld breaks size: %ld\n", samples_t.cols(), breaks.size());

    printf("Setting up FOH\n");
    drake::trajectories::PiecewisePolynomial<double> initial_traj =
            drake::trajectories::PiecewisePolynomial<double>::FirstOrderHold(breaks, samples_t);
    printf("Setting up initial traj\n");
    dircol.SetInitialTrajectory(
            drake::trajectories::PiecewisePolynomial<double>(),
            initial_traj
    );

    printf("Starting solve... ");
    drake::solvers::MathematicalProgramResult result = drake::solvers::Solve(dircol);
    printf("Done!\n");

    bool solve_sucess = result.is_success();

    printf("Solve success? %d\n", solve_sucess);
    printf("Solver used: %s\n", result.get_solver_id().name().c_str());

    sys.ToAutoDiffXd();

    return 0;

}