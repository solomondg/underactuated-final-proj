//
// Created by solomon on 5/31/21.
//

#include <drake/common/drake_copyable.h>
#include <drake/systems/framework/basic_vector.h>
#include <drake/systems/framework/context.h>
#include <drake/systems/framework/continuous_state.h>
#include <drake/systems/framework/leaf_system.h>
#include <drake/systems/framework/framework_common.h>
#include <drake/systems/framework/vector_base.h>
#include <drake/common/default_scalars.h>

#pragma once

template<typename T>
class RobotSystem final : public drake::systems::LeafSystem<T> {
public:
    DRAKE_NO_COPY_NO_MOVE_NO_ASSIGN(RobotSystem);

    RobotSystem(): RobotSystem(drake::systems::SystemTypeTag<RobotSystem>{}) {};

    template <typename U>
    explicit RobotSystem(const RobotSystem<U>& other) : RobotSystem<T>() {};
    ~RobotSystem() override;

protected:
    explicit RobotSystem(drake::systems::SystemScalarConverter converter);

private:

    void CopyStateOut(const drake::systems::Context<T> &context,
                      drake::systems::BasicVector<T> *output) const;

    void DoCalcTimeDerivatives(
            const drake::systems::Context<T> &context,
            drake::systems::ContinuousState<T> *derivatives) const override;

    //void f(const drake::systems::VectorBase<T> &x,
    //       const drake::systems::VectorBase<T> &u,
    //       drake::systems::VectorBase<T> &dx) const;
    void f(const drake::Vector6<T> &x, const drake::Vector2<T> &u, drake::Vector6<T> *dx) const;

    template <typename> friend class RobotSystem;
};

